import darksim.framework as framework
import darksim.dmath as dmath
import darksim.utils as utils
import flowpm
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.animation as mpl_anim
from matplotlib import rc
import time
import tensorflow as tf
plt.rcParams["figure.figsize"] = (20,20)
st = time.time()
uni = framework.ModelUniverse(w=-1.1, vis_matter=0.06, dark_matter=0.24, dark_energy=0.67, radiation=8.4e-5, lam=0.0, use_default=False)
# sim = uni.simulate(0.01, 1, 20, N_mesh=32)
uni.info = {
    'L0': 100,
    'steps':150,
    'N_mesh': 256
}

def animateDensity(uni, fps=10, update_axis=True, title='', save=True, filename=r"/home/dev/{}.mp4", dpi=400, **kwargs):
    # Plot parameters
    # Axis limits
    lim = uni.info['L0'] / 2

    fig = plt.gcf()
    ax = plt.axes()

    # Fonts
    rc('font',size=12)
    rc('font',family='serif')
    rc('axes',labelsize=8)

    # Resolution
    # rc('figure.figsize = (15, 15)')
    # Titles
    if title == '':
        title = str(uni) + ' [$M_{\odot}/$Mpc$^3$]'

    ax.set_title(title)
    ax.set_xlabel('x [Mpc]')
    ax.set_ylabel('y [Mpc]')

    _res = uni.read_time_step(uni.info['steps'] - 1)

    paint = flowpm.cic_paint(
        tf.zeros_like(np.ndarray((2,uni.info['N_mesh'],uni.info['N_mesh'],uni.info['N_mesh']), dtype=np.float32)), _res[0,:,:,:]
    )[0].numpy().sum(axis=0)

    _min, _max = np.min(paint), np.max(paint)

    # Find 90% of points, so as much detail as possible can be seen
    vmin = _min # Don't change vmin, leads to good detail
    vmax = _max
    length = paint.shape[0]
    while True:
        size = np.where(paint > vmax)[0].shape[0]


        if size >= length * .90: # Take only [:90%] of elements
            break

        vmax -= 1

    def _updateDensity(i):
        im.set_array(
            flowpm.cic_paint(
                tf.zeros_like(np.ndarray((2,uni.info['N_mesh'],uni.info['N_mesh'],uni.info['N_mesh']), dtype=np.float32)), uni.read_time_step(i)[0,:,:,:]
            )[0].numpy().sum(axis=0)
        )
        return im,

    # Initial plot
    im = plt.imshow(
        flowpm.cic_paint(
            tf.zeros_like(np.ndarray((2,uni.info['N_mesh'],uni.info['N_mesh'],uni.info['N_mesh']), dtype=np.float32)), _res[0,:,:,:])[0].numpy().sum(axis=0),
        interpolation='lanczos', cmap='inferno', vmin=vmin, vmax=vmax
    )
    plt.colorbar()

    anim = mpl_anim.FuncAnimation(fig, _updateDensity, uni.info['steps'], interval=1000/fps, **kwargs)

    if save:
        # writer = mpl_anim.PillowWriter(fps=30) # gif
        writer = mpl_anim.FFMpegWriter(fps=fps) # mp4
        anim.save(filename.format('IMSHOWTEST_256_' + str(uni)), writer=writer, dpi=dpi)
    return anim

anim_obj = animateDensity(uni,)
print(f"Total Runtime: {time.time() - st}")
