import darksim.framework as framework
import darksim.dmath as dmath
import darksim.utils as utils
import flowpm
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.animation as mpl_anim
from matplotlib import rc
import time
plt.rcParams["figure.figsize"] = (32,18)
st = time.time()
uni = framework.ModelUniverse(w=-1.1, vis_matter=0.06,dark_matter=0.24, de_type='CC',dark_energy=0.67, radiation=8.4e-5, lam=0.0, use_default=False)
# sim = uni.simulate(np.linspace(0.01, 1, 20), 20, N_mesh=32)
uni.info = {
    'L0': 100,
    'steps': 149,
    'N_mesh': 256
}
import numpy as np
import numpy as np
import matplotlib.pyplot as plt

from darksim.utils import const

def a(Ht, zeta0, zeta1): # times in Gyr
    # Ignore errors, this just means the universe hasn't been born
    with np.errstate(invalid='ignore'):
        return np.power((zeta0 + zeta1 - 3 + (3 - zeta1) * np.exp(0.5 * zeta0 * Ht)) * (1 / zeta0), 2 / (3 - zeta1))

Htimes = np.linspace(-.8, 2, 150)
sf = a(Htimes, 4.38929, -2.16673)
sf *= (1/sf[0]) # Normalize
print(sf)
uni.sf = sf
uni.DIR = '/mnt/wolf/darksim_data/'
def animate3d(uni, fps=10, update_axis=True, title='', save=True, filename=r"/home/dev/animations/{}.mp4", dpi=400, **kwargs):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    scatter, = ax.plot([], [], [], "o", markersize=.075, linewidth=2, color='black', label='points', clip_on=False)

    # Plot parameters
    # Axis limits
    _res = uni.read_time_step(0)
    _min, _max = np.min(_res[0,0,:,0]), np.max(_res[0,0,:,0])

    # Coefficient to multiply by and offset
    coeff = uni.info['L0'] / (_max - _min)
    offset = (_max - _min) / 2

    lim = uni.info['L0'] / 2
    ax.set_xlim(-lim,lim)
    ax.set_ylim(-lim,lim)
    ax.set_zlim(-lim,lim)

    # Fonts
    rc('font',size=16)
    rc('font',family='serif')
    rc('axes',labelsize=16)

    # Titles
    ax.set_title(f'Model Universe, $\Omega_{{M,0}}=$1, $\Omega_{{r,0}}=$0, $\Omega_{{DE,0}}=$0, $\zeta_0=$4.389, $\zeta_1=$-2.167 (Bulk Fluid)')
    ax.set_xlabel('x [Mpc]', labelpad=10)
    ax.set_ylabel('y [Mpc]', labelpad=10)
    ax.set_zlabel('z [Mpc]', labelpad=10)

    # sft = fig.text(0, 0, "a=1", verticalalignment='center', horizontalalignment='center', transform=ax.transAxes)
    sf_subtitle = fig.suptitle(f'a = {round(1, 2)}', y=0.87)

    # Remove background
    ax.grid(False)
    ax.xaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
    ax.yaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
    ax.zaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))

    # Ticks
    ax.xaxis._axinfo['tick']['inward_factor'] = .4
    ax.xaxis._axinfo['tick']['outward_factor'] = 0.2
    ax.yaxis._axinfo['tick']['inward_factor'] = .4
    ax.yaxis._axinfo['tick']['outward_factor'] = 0.2
    ax.zaxis._axinfo['tick']['inward_factor'] = .4
    ax.zaxis._axinfo['tick']['outward_factor'] = 0.2
    ax.zaxis._axinfo['tick']['outward_factor'] = 0.2
    uni.sf += 1
    def _update3d(i):
        result = uni.read_time_step(i)
        sf_subtitle.set_text(f'a={round(uni.sf[i], 2)}')
        scatter.set_data((result[0,0,:,0] - offset)*uni.sf[i], (result[0,0,:,1] - offset)*uni.sf[i])
        scatter.set_3d_properties((result[0,0,:,2] - offset)*uni.sf[i])


    anim = mpl_anim.FuncAnimation(fig, _update3d, uni.info['steps'], interval=1000/fps,
                                 blit=False, cache_frame_data=False, **kwargs)

    if save:
        # writer = mpl_anim.PillowWriter(fps=10) # gif
        writer = mpl_anim.FFMpegWriter(fps=10) # mp4
        anim.save(filename.format('final' + str(uni)), writer=writer, dpi=dpi)
    return anim

anim_obj = animate3d(uni, fps=10, title=str(uni), save=False)
plt.show()
print(f"Total Runtime: {time.time() - st}")
