#!/usr/bin/env python
from setuptools import setup

setup(
    name='Dark Energy and Dark Matter Simulations',
    version='0.0.1',
    packages=['darksim'],
    description='Simulate evolution of model universes and generate visuals',
    url='https://gitlab.com/phys3266/darksim',
    author='Carson West, Carol-Anne Collins, Michael Santrock, Rohan Srivastava, Ruike Li',
    license='MIT',
    author_email='',
    install_requires=['numpy', 'tensorflow', 'flowpm', 'scipy'],
    keywords='univese dark matter energy phantom model evolve simulate'
)
