import numpy as np
import pandas as pd

import dask.distributed as dask_dist
import dask.dataframe as dd
import dask.array as da

# import darksim.dmath as dmath
# import darksim.utils as utils
# import darksim.fancy as fancy
import time
# Temp imports
import darksim.dmath as dmath
import darksim.utils as utils
import darksim.fancy as fancy

from scipy.interpolate import InterpolatedUnivariateSpline as iuspline
import tensorflow.compat.v1 as tf
import flowpm
from flowpm import linear_field, lpt_init, nbody, cic_paint
import pickle
import os


class ModelUniverse:
    """ Create a model universe from density parameters. This universe can be time evolved

    :args:
    data_file_name (str): Name of .csv file with initial data. See utils.DataHandler class for details on file use.

    :kwargs: All kwargs are density parameters to be used when constructing the Friedmann Equation
    vis_matter (float, 0 < matter < 1): Matter density parameter for visible matter only
    dark_matter (float, 0 < dark_matter < matter): Dark matter density parameter for dark matter only
    radiation (float, 0 < radiation < 1): Radiation density parameter
    lambda (float, 0 < lambda < 1): Cosmological constant parameter

    """
    ingredients = ['vis_matter', 'dark_matter', 'radiation', 'dark_energy']

    # Directory used to save data
    DIR = 'data/{uni}/'

    flow_cosmo = {
        "w0": -1.0,
        "wa": 0.0,
        "H0": 100,
        "h": 0.6774,
        "Omega0_b": 0.04860,
        "Omega0_c": 0.2589,
        "Omega0_m": 0.3075,
        "Omega0_k": 0.0,
        "Omega0_de": 0.6925,
        "n_s": 0.9667,
        "sigma8": 0.8159
    }
    def __init__(self, de_type, w=0, use_default=False, DIR='', **kwargs):
        self.de_w = w
        self.density_params = kwargs

        self._vm = self.density_params['vis_matter']
        self._dm = self.density_params['dark_matter']
        self._matter = self._vm + self._dm
        self._r = self.density_params['radiation']
        self._de = self.density_params['dark_energy']

        self.de_type = de_type

        # Verify density params
        for key,value in self.density_params.items():
            if value < 0 or value > 1:
                raise ValueError(f"Density parameter {key}={value} is not in valid range of 0 < {key} <= 1")

        if de_type == '':
            raise ValueError(f'Must give type of dark energy')

        # Get total density (this accounts for curvature in FE)
        # self.total_density = sum(self.density_params.values())
        self.total_density = 1. # Assume current universe is flat

        # For ingredients not given, set equal to 0
        for ing in self.ingredients:
            if ing not in self.density_params.keys():
                self.density_params[ing] = 0.0

        # Create flowpm cosmology object for compatibiltiy
        if not use_default:
            self.flow_cosmo = {
                "w0": self.de_w,
                "wa": 0.0,
                "H0": utils.const.H_0h,
                "h": utils.const.h,
                "Omega0_b": self._vm,
                "Omega0_c": self._dm,
                "Omega0_m": self._matter,
                "Omega0_k": 0.0,
                "Omega0_de": self._de,
                "n_s": 0.9667,
                "sigma8": 0.8159
            }

        if DIR == '':
            self.DIR = self.DIR.format(uni=str(self))
        else:
            self.DIR = DIR

        # Create dir if it doesnt exist
        if not os.path.exists(self.DIR):
            os.makedirs(self.DIR)

    def __str__(self):
        return f'ModelUniverse_Omb0={self._vm}_Omd0={self._dm}_Or0={self._r}_DET={self.de_type}_Ode0={self._de}_DEEOS={self.de_w}'

    @staticmethod
    def _dark_energy_func(a, w):
        """ Return dark energy density function with equation of state paramter w"""
        if w < -1:
            return np.power(a, 3*abs(1+w))
        elif w < -1/3 and w >= -1: # Quintessence
            return np.power(a, -3*(1+w))
        elif w == 0: # No dark energy
            return 0
        elif w == -1: # CC
            return 1.
        else: # Not valid equation of state parameter
            raise ValueError(f'w = {w} is not a valid value for dark energy equation of state parameter')


    def _friedmann_dist(self, z):
        """ Constructs callable function of right side of friedmann equation where left side is H(z). Use this
        as a base for all friedmann equation calculations with approiate coefficients applied"""
        return utils.const.c * np.power(utils.const.H_0, -1) * np.power(self._r * np.power(1+z, 4) + (self._vm+self._dm) * np.power(1+z, 3) + self._dark_energy_func(z, self.de_w), -0.5)

    def _friedmann_age(self, z):
        """ Constructs callable function of right side of friedmann equation where left side is H(z). Use this
        as a base for all friedmann equation calculations with approiate coefficients applied"""
        return 1e6*np.power(utils.const.H_0 * (1+z) * np.power(self._r * np.power(1+z, 4) + (self._vm+self._dm) * np.power(1+z, 3) + self._dark_energy_func(z, self.de_w), 0.5), -1)

    def E(self, a):
        return np.sqrt(self._r * np.power(a, -4) + self._matter + np.power(a, -3) + self._de * self._dark_energy_func(a, self.de_w))

    def _write_time_step(self, data, i, filename_suffix=''):
        filename = str(self) + f'__{i}' + filename_suffix
        pickle.dump(data, open(self.DIR + filename, 'wb'))
        return filename

    def read_time_step(self, i, filename_suffix=''):
        return pickle.load(open(self.DIR + str(self) + f'__{i}' + filename_suffix, 'rb'))

    @staticmethod
    def _load_inital_data(self, file_name):
        pass

    @staticmethod
    def translate_array(positions):
        pass

    def simulate(self, a, steps, N_mesh=32, L0=100, batch_size=2, mesh=False, write=True, init_cond=None, **kwargs):
        """
        Evolves universe in time from a0 to af (scale factors)

        Wrapper function that calls _time_step (end - 0) / step_size times and handles the 3d array of time evolved data.
        """
        newest_particle_state = None
        # Construct parameters for time evolution
        self.info = {
            'a0': a[0],
            'af': a[-1],
            'a': a,
            'steps': steps,
            'N_mesh': N_mesh,
            'L0': L0,
            'batch_size': batch_size,
            'steps_taken': 0,
            'cosmo': self.flow_cosmo,
            **kwargs
        }

        # Times of evolution, normalized to af=1, with length of steps
        scale_factors = (a / np.max(a))[::round(a.shape[0]/steps)]
        self.info['sfn'] = scale_factors

        # Power spectrum - describes the 'clustering' of the universe at inital epoch a0 based on Plank 2015 observations
        klin = np.loadtxt('model_parameters/Planck15_a1p00.txt').T[0]
        plin = np.loadtxt('model_parameters/Planck15_a1p00.txt').T[1]

        # Interpolate data for a more continious graph
        ipklin = iuspline(klin, plin)

        ## Initial conditions
        # Gaussian distribution of mass of shape (batch_size, N_mesh, N_mesh, N_mesh). Gives weighting to
        # each mesh point basically
        # If init_cond is given, use it
        # if init_cond is not None:
        #     init_cond = init_cond
        # else:
        init_cond = flowpm.linear_field(N_mesh, L0, ipklin, batch_size=batch_size)

        # Write init_cond to file
        self._write_time_step(init_cond, 0, filename_suffix='init_cond')

        # Add initial conditions to info
        # self.info['init_cond'] = init_cond

        # Initial particle conditions
        # Tensor of shape (3, batch_size, N_mesh^3, 3)
        # First axis is (position, velocity, acceleration) and last axis is copy of position in (x,y,z) format
        # Second axis chooses which batch to pull from.
        newest_particle_state = flowpm.lpt_init(init_cond, a=a[0])

        # Write this initial state to file
        self._write_time_step(newest_particle_state, 0)

        # Initial conditions mesh is just the particles
        # mesh_states.append(particle_states[0])

        # Now actually evolve
        for i,a in enumerate(scale_factors[:-1]):
            # Evolve one step at a time - this takes into account DE, gravity, etc. then apply expansion
            newest_particle_state = flowpm.nbody(newest_particle_state, scale_factors[i:i+2], N_mesh, cosmology=self.flow_cosmo)
            # newest_particle_state = utils.nbody(newest_particle_state, scale_factors[i:i+2], N_mesh, cosmology=self.flow_cosmo)

            # Apply expansion

            # Write to file each step
            self._write_time_step(newest_particle_state, i+1)

            self.info['steps_taken'] += 1
            print(f"[{self}][{N_mesh}, {batch_size}, {scale_factors[0]}-{scale_factors[-1]}] Time Evolution: {self.info['steps_taken']}/{self.info['steps']-1}\t\t", end='\n')

        # Write info
        self._write_time_step(self, '', filename_suffix='uniobj')

        return self.info
        # return init_cond

    def death_of_universe(self, scale_factor, entropy):
        """ Find how the universe will die """
