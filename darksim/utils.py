import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.animation as mpl_anim
from matplotlib import rc
import time
import tensorflow as tf
import flowpm

from matplotlib import rc
plt.rcParams["figure.figsize"] = (10, 10)

class const:
    c = 2.99792458e8 # speed of light, m/s
    H_0 = 70e3 # hubble constant, s^-1 m/Mpc
    H_0h = 100 # hubble constant, s^-1 km/Mpc/h
    h = 0.6774 # Not Plank's h!

    H_0_si = H_0 * 3.086e+22

    G = 6.67e-11


""" Animations """
def animate3d(uni, fps=10, update_axis=True, title='', save=True, filename=r"{}.mp4", dpi=200, **kwargs):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    scatter, = ax.plot([], [], [], "o", markersize=1, linewidth=2, color='black', label='points', clip_on=False)

    # Plot parameters
    # Axis limits
    _res = uni.read_time_step(0)
    _min, _max = np.min(_res[0,0,:,0]), np.max(_res[0,0,:,0])

    # Coefficient to multiply by and offset
    coeff = uni.info['L0'] / (_max - _min)
    offset = (_max - _min) / 2

    lim = uni.info['L0'] * 3
    ax.set_xlim(-lim,lim)
    ax.set_ylim(-lim,lim)
    ax.set_zlim(-lim,lim)

    # Fonts
    rc('font',size=16)
    rc('font',family='serif')
    rc('axes',labelsize=16)

    # Titles
    # ax.set_title(f'Model Universe, $\Omega_{{M,0}}=$0.27, $\Omega_{{r,0}}=$8.5e-5, $\Omega_{{DE,0}}=$0.72 (Cosmological Constant)')
    # ax.set_title(f'Model Universe {title}, $\Omega_{{M,0}}=${uni._matter}, $\Omega_{{r,0}}=${uni._r}, $\Omega_{{de}}=${uni._de}, $\omega_{{de}}=${uni.de_w}')
    ax.set_xlabel('x [Mpc]', labelpad=10)
    ax.set_ylabel('y [Mpc]', labelpad=10)
    ax.set_zlabel('z [Mpc]', labelpad=10)

    # sft = fig.text(0, 0, "a=1", verticalalignment='center', horizontalalignment='center', transform=ax.transAxes)
    sf_subtitle = fig.suptitle(f'a = {round(1, 2)}, $H_0(t-t_0)=${round(-.8, 2)}', y=0.87)

    # Remove background
    ax.grid(False)
    ax.xaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
    ax.yaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
    ax.zaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))

    # Ticks
    ax.xaxis._axinfo['tick']['inward_factor'] = .4
    ax.xaxis._axinfo['tick']['outward_factor'] = 0.2
    ax.yaxis._axinfo['tick']['inward_factor'] = .4
    ax.yaxis._axinfo['tick']['outward_factor'] = 0.2
    ax.zaxis._axinfo['tick']['inward_factor'] = .4
    ax.zaxis._axinfo['tick']['outward_factor'] = 0.2
    ax.zaxis._axinfo['tick']['outward_factor'] = 0.2

    def _update3d(i):
        result = uni.read_time_step(i)
        sf_subtitle.set_text(f'a={round(uni.sf[i], 2)}, $H_0(t-t_0)=${round(Htimes[i], 2)}')
        scatter.set_data((result[0,0,:,0] - offset)*coeff*uni.sf[i], (result[0,0,:,1] - offset)*coeff*uni.sf[i])
        scatter.set_3d_properties((result[0,0,:,2] - offset)*coeff*uni.sf[i])


    anim = mpl_anim.FuncAnimation(fig, _update3d, uni.info['steps'], interval=1000/fps,
                                 blit=False, cache_frame_data=False, **kwargs)

    if save:
        # writer = mpl_anim.PillowWriter(fps=10) # gif
        writer = mpl_anim.FFMpegWriter(fps=10) # mp4
        prefix = ''
        print('SAVE_DIR: ', filename.format(prefix + str(uni)))
        anim.save(filename.format(prefix + str(uni)), writer=writer, dpi=dpi)
    return anim


def animateDensity(uni, fps=10, update_axis=True, title='', save=True, filename=r"{}.mp4", dpi=200, **kwargs):
    # Plot parameters
    # Axis limits
    lim = uni.info['L0'] / 2

    fig = plt.gcf()
    ax = plt.axes()

    # Fonts
    rc('font',size=16)
    rc('font',family='serif')
    rc('axes',labelsize=16)

    # Resolution
    # rc('figure.figsize = (15, 15)')
    # Titles
    if title == '':
        title = f'Model Universe Density Mesh, $\Omega_{{M,0}}=$0.27, $\Omega_{{r,0}}=$8.5e-5, $\Omega_{{DE,0}}=$0.72 (Cosmological Constant)'

    ax.set_title(title)
    ax.set_xlabel('x [index]', fontsize=16)
    ax.set_ylabel('y [index]', fontsize=16)

    _res = uni.read_time_step(uni.info['steps'] - 1)

    paint = flowpm.cic_paint(
        tf.zeros_like(np.ndarray((2,uni.info['N_mesh'],uni.info['N_mesh'],uni.info['N_mesh']), dtype=np.float32)), _res[0,:,:,:]
    )[0].numpy().sum(axis=0)

    _min, _max = np.min(paint), np.max(paint)

    # Find 90% of points, so as much detail as possible can be seen
    vmin = _min # Don't change vmin, leads to good detail
    vmax = _max
    length = paint.shape[0]
    while True:
        size = np.where(paint > vmax)[0].shape[0]


        if size >= length * .85: # Take only [:90%] of elements
            break

        vmax -= 1

    def _updateDensity(i):
        im.set_array(
            flowpm.cic_paint(
                tf.zeros_like(np.ndarray((2,uni.info['N_mesh'],uni.info['N_mesh'],uni.info['N_mesh']), dtype=np.float32)), uni.read_time_step(i)[0,:,:,:]*uni.sf[i]
            )[0].numpy().sum(axis=0)
        )
        return im,

    # Initial plot
    im = plt.imshow(
        flowpm.cic_paint(
            tf.zeros_like(np.ndarray((2,uni.info['N_mesh'],uni.info['N_mesh'],uni.info['N_mesh']), dtype=np.float32)), _res[0,:,:,:])[0].numpy().sum(axis=0),
        interpolation='lanczos', cmap='inferno', vmin=vmin, vmax=vmax, origin='lower'
    )
    plt.colorbar(label='[$10^{18} M_{\odot}/$Mpc$^3$]')

    anim = mpl_anim.FuncAnimation(fig, _updateDensity, uni.info['steps'], interval=1000/fps, **kwargs)

    if save:
        # writer = mpl_anim.PillowWriter(fps=30) # gif
        writer = mpl_anim.FFMpegWriter(fps=10) # mp4
        print('SAVE_DIR: ', filename.format(str(uni)))
        anim.save(filename.format(str(uni)), writer=writer, dpi=dpi)
    return anim


def animateScaleTime(Htimes, fxn, fxn_args={}, fps=30, update_axis=True, title='', save=True, filename=r"/home/rettwolf/projects/PHYS3266/animations/{}.gif", dpi=400, **kwargs):
    fig, ax = plt.subplots()

    line, = ax.plot([], [], color='black')

    # Fonts
    rc('font',size=16)
    rc('font',family='serif')
    rc('axes',labelsize=16)

    # Titles
    # ax.set_title(f'Model Universe, $\Omega_{{M,0}}=$0.27, $\Omega_{{r,0}}=$8.5e-5, $\Omega_{{DE,0}}=$0.72 (Cosmological Constant)')
    ax.set_title(f'Scale factor vs time {title}')
    ax.set_xlabel('Time [$H_0(t - t_0)$]', labelpad=10)
    ax.set_ylabel('$a(t)$', labelpad=10)
    ax.set_xlim(-1, 2)
    ax.set_ylim(0, 2.5)
    # sf_subtitle = fig.suptitle(f'$\zeta_1=${zeta1[0]}', y=0.87)

    def init():
        line.set_data([], [])
        return line,

    def _update(i):
        line.set_data(Htimes, fxn(**fxn_args))
        # sf_subtitle.set_text(f'$\zeta_1=${round(zeta1[i], 2)}')
        return line,
    # ax.plot(Htimes, a(Htimes, 4, -1), color='cyan', linestyle='dashed', label='$\zeta_0=4, \zeta_1=-1$ (de Sitter)')
    # ax.plot(Htimes, a(Htimes, 8, 2), color='blue', linestyle='dotted', label='$\zeta_0=8, \zeta_1=-2$ (No Big Bang)')
    # ax.plot(Htimes, a(Htimes, 4.389, -2.166), color='gold', linestyle='dashdot', label='$\zeta_0=4.389, \zeta_1=-2.166$ (Our Universe)')

    ax.axvline(x=0, linewidth=0.5)
    ax.axhline(y=1, linewidth=0.5)

    ax.legend(loc='best')
    plt.show()
    anim = mpl_anim.FuncAnimation(fig, _update, steps-1, init_func=init, interval=1, blit=True, **kwargs)

    if save:
        writer = mpl_anim.PillowWriter(fps=30) # gif
        # writer = mpl_anim.FFMpegWriter(fps=15) # mp4
        print('SAVE_DIR: ', filename.format('test_anim_sf_zeta1'))
        anim.save(filename.format('test_anim_sf_zeta1'), writer=writer, dpi=60)
    return anim




def center_data(result, L0, batch=0):
    return result[0,batch,:,:] - (0.5*L0)


def gen_test_data(n=100, clusters=2, scale=1e21, v_scale=1e7, m_scale=1e35):
    """ Generate test data for simulation

    Generates (x, y, z) position components, (v_x, v_y, v_z) velocity components, and masses.
    Positions follow the structure seen in superclusters, where regions of densly packed objects
    in a roughly spherical distribution, with decreasing density as R increases, where each of
    these `clusters` are seperated by a relativity large distance. Velocities follow a similar
    pattern where closer to R = 0 velocities are lower and increase logarithmically. Masses
    are calculated proportionally to their velocity magnitude.

    Scale values are meant to represent accurate scales at the simulation scale in SI units. For
    exampe, scale is meant to be on the scale of 100,000 light years, velocity on the scale of
    10,000 km/s, and mass on 100,000 solar mass scales for default values.

    n (int, 100): Number of objects per cluster to calculate.
    clusters (int, 2): Number of clusters to calculate. Total number of objects will be
        n * clusters
    scale (float, 1e21): Position scale
    v_scale (float, 1e7): Velocity scale
    m_scale (float, 1e35): Mass scale

    """
    gen = lambda offset: np.random.rayleigh(scale, n) * np.random.normal(0, 100, size=n) * (np.random.random(size=n)) + offset

    # Positions
    tempx = []
    tempy = []
    tempz = []
    tempvx = []
    tempvy = []
    tempvz = []
    for i in range(clusters):
        if i == 0:
            offset = 0
        elif i % 2 == 1:
            offset = scale * 500 / i
        else:
            offset = -scale * 750 / i
        print(offset)
        x = tempx.append(gen(offset))
        y = tempy.append(gen(offset))
        z = tempz.append(gen(offset))

    x = np.concatenate(tuple(tempx))
    y = np.concatenate(tuple(tempy))
    z = np.concatenate(tuple(tempz))

    xyz = np.column_stack((x,y,z))

    # Velocities
    vx = x.copy() * (v_scale - np.random.standard_gamma(n*3)) * (v_scale / scale)
    vy = y.copy() * (v_scale - np.random.standard_gamma(n*3)) * (v_scale / scale)
    vz = z.copy() * (v_scale - np.random.standard_gamma(n*3)) * (v_scale / scale)

    m = np.power(np.power(vx, 2) + np.power(vx, 2) + np.power(vx, 2), 1/2) * m_scale

    return [xyz, np.array([vx,vy,vz]), np.array(m)]
