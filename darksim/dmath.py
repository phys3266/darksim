import numpy as np
from scipy.integrate import odeint
import darksim.utils as utils
import tensorflow as tf

from flowpm.tfbackground import f1, E, f2, Gf, gf, gf2, D1, D2, cosmo, D1f
from flowpm.utils import white_noise, c2r3d, r2c3d, cic_paint, cic_readout
from flowpm.kernels import fftk, laplace_kernel, gradient_kernel, longrange_kernel

class integrate:
    """ Integration methods """
    def _gaussxw(N, epsilon):
        # Initial approximation to roots of the Legendre polynomial
        a = np.linspace(3,4*N-1,N)/(4*N+2)
        x = np.cos(np.pi*a+1/(8*N*N*np.tan(a)))

        # Find roots using Newton's method
        delta = 1.0
        while delta>epsilon:
            p0 = np.ones(N,float)
            p1 = np.copy(x)
            for k in range(1,N):
                p0,p1 = p1,((2*k+1)*x*p1-k*p0)/(k+1)
            dp = (N+1)*(p0-x*p1)/(1-x*x)
            dx = p1/dp
            x -= dx
            delta = max(abs(dx))

        # Calculate the weights
        w = 2*(N+1)*(N+1)/(N*N*(1-x*x)*dp*dp)

        return x, w


    def gauss(fxn, a, b, steps, epsilon=1e-15):
        # Get points and weights
        points, weights = integrate._gaussxw(steps, epsilon)

        # Scale points and weights
        points, weights = 0.5*(b-a)*points+0.5*(b+a), 0.5*(b-a)*weights

        return np.sum(fxn(points) * weights)

class derivative:
    pass

class scalefactor:
    # Methods for different ways of calculating scale factors for different models

    # Bulk Viscosity, matter dominated model
    def bulk_viscosity(Ht, zeta0=1.98, zeta1=0):
        """ Bulk Viscosity model of matter dominated universe. Default values for zeta are best fit


        See: https://arxiv.org/abs/1002.3605

        """
        # / TODO / Add RK4 method to solve ODE
        # Ignore errors, this just means the universe hasn't been born
        with np.errstate(invalid='ignore'):
            return np.power((zeta0 + zeta1 - 3 + (3 - zeta1) * np.exp(0.5 * zeta0 * Ht)) * (1 / zeta0), 2 / (3 - zeta1))


    def dark_energy(Htimes, E, a0=0.01):
        # / TODO / Add RK4 method to solve ODE
        return odeint(E, a0, Htimes).flatten()


def get_dist(fed, z):
    """ Returns a matrix of proper, angular, and luminosity distances

    z: array of redshifts (Nx1)
    d[:,1] is proper distance
    d[:,2] is angular distance
    d[:,3] is luminosity distance
    """
    r0 = np.zeros_like(z)
    sum = 0
    for i in range(1, len(z)):
        sum += integrate.gauss(fed, z[i-1], z[i], 10)
        r0[i] = sum

    return np.column_stack((z, r0, r0/(1+z), r0*(1+z)))


def get_age(fea, z):
    age = np.zeros_like(z)
    sum = integrate.gauss(fea, z[0], 100_000, 10_000)
    for i in range(len(z)-1, 0, -1):
        sum += integrate.gauss(fea, z[i], z[i-1], 10)
        age[i] = sum
    return np.column_stack((z, age))





def get_acceleration(positions, mass, softening=0.1):
    """ Returns acceleration components (a_x,a_y,a_z) in Nx3 matrix

    position: Nx3 matrix of positions representing (x,y,z), floats
    mass: Nx1 vector of the body's masses, floats
    G: Gravitational constant, float
    softening: Value used to avoid error of close objects, float
    """
    # positions
    x = positions[:,0:1]
    y = positions[:,1:2]
    z = positions[:,2:3]

    # r_j - r_i. Object seperation
    dx = x.T - x
    dy = y.T - y
    dz = z.T - z

    # inverse cube of distances
    inv_r3 = (dx**2 + dy**2 + dz**2 + softening**2)**(-1.5)

    ax = utils.const.G * (dx * inv_r3) * mass
    ay = utils.const.G * (dy * inv_r3) * mass
    az = utils.const.G * (dz * inv_r3) * mass

    # pack together the acceleration components
    a = np.hstack((ax,ay,az))

    return a


def apply_acceleration(acc, positions, velocities, method='fast'):
    pass


def gravitational_force(position, mass, softening, dt):
    """ Returns 3D matrix of positions through time, a vector of corresponding times
    and a vector of the accelerations at each time

    position: Nx3 matrix of positions representing (x,y,z), floats
    mass: N value vector of the body's masses, floats
    softening: Value used to avoid error of close objects, float
    Nt: Number of time steps, int
    dt: Change in time between steps, int
    """
    N = position.shape[0]

    posprog = np.zeros((N,3))
    posprog[:,0] = position
    acc = get_acceleration(position, mass, utils.const.G, softening)
    velocity = np.zeros((N,3))
    # 1st 1/2 kick
    # / TODO / Use relativistic velocity addition
    velocity += acc * dt/2.0

    # drift
    posprog[:,i] = posprog[:,i-1] + velocity * dt

    # update acceleration
    acc = get_acceleration(position, mass, utils.const.G, softening)

    # 2nd 1/2 kick
    # / TODO / Use relativistic velocity addition
    velocity += acc * dt/2.0

    return posprog, acc


def nbody(state, stages, N_mesh, cosmology=cosmo, pm_N_mesh_factor=1, name="NBody"):
  """
  Integrate the evolution of the state across the givent stages

  Parameters:
  -----------
  state: tensor (3, batch_size, npart, 3)
    Current step for bodies

  stages: np.array (N,)
    Scale Factors

  N_mesh: int, or list of ints
    Number of cells

  pm_N_mesh_factor: int
    Upsampling factor. 1 is default (no upsampling)

  Returns
  -------
  state: tensor (3, batch_size, npart, 3)
    Integrated state to last scale factor
  """
  with tf.name_scope(name):
    state = tf.convert_to_tensor(state, name="state")

    shape = state.get_shape()
    if isinstance(N_mesh, int):
      N_mesh = [N_mesh, N_mesh, N_mesh]

    # Unrolling leapfrog integration to make tf Autograph happy
    if len(stages) == 0:
      return state

    ai = stages[0]

    # first force calculation for jump starting
    state = force(state, N_mesh, pm_N_mesh_factor=pm_N_mesh_factor, cosmology=cosmology)

    x, p, f = ai, ai, ai
    # Loop through the stages
    for i in range(len(stages) - 1):
      a0 = stages[i]
      a1 = stages[i + 1]
      ah = (a0 * a1)**0.5

      # Kick step
      state = kick(state, p, f, ah)
      p = ah

      # Drift step
      state = drift(state, x, p, a1)
      x = a1

      # Force
      state = force(state, N_mesh, pm_N_mesh_factor=pm_N_mesh_factor)
      f = a1

      # Kick again
      state = kick(state, p, f, a1)
      p = a1

      state = state[0,:,:,:] * a0

    return state
